/*
    TubiTui - A lightweight, libre, TUI-based YouTube client
    Copyright (C) 2021 777 <g1t_777@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
*/

use std::{fs, process::Command, thread, time};

use cursive::{
    Cursive,
    event::Key::{Enter, Esc},
    theme::{BorderStyle, Color::*, Palette, PaletteColor::*, Theme},
    view::{Nameable, Resizable, Scrollable},
    views::{Button, Checkbox, Dialog, DummyView, EditView, LinearLayout, RadioGroup, SelectView, TextView}
};
use sanitise_file_name::sanitise;

use channel::ChannelInfo;
use comments::CommentsPage;
use config::Config;
use instance::Instance;
use local_playlist::LocalPlaylist;
use playlist::PlaylistInfo;
use search::{ChannelResult, PlaylistResult, SearchResult, VideoResult};
use utils::{determine_path, seconds_to_minutes, UData};
use video::{VideoFormats, VideoInfo};

mod channel;
mod comments;
mod config;
mod instance;
mod local_playlist;
mod playlist;
mod search;
mod utils;
mod video;

fn main() {
    let mut proc = cursive::default();
    proc.clear_global_callbacks(Esc);
    proc.add_layer(
        Dialog::new()
            .title("TubiTui")
            .button("Search", search)
            .button("Saved", playlists_list)
            .button("Settings", settings),
    );
    proc.add_global_callback('q', |p| {
        p.quit();
    });
    proc.add_global_callback('m', |p| {
        p.dump();
        p.quit();
        main();
    });
    let config = match Config::read() {
        Ok(c) => c,
        Err(e) => {
            proc.add_layer(Dialog::text(format!("Error reading config file: {:?}", e)));
            Config::default()
        }
    };
    if config.dark_mode {
        let mut palette = Palette::default();
        let colours = vec![
            (Background, Rgb(44, 57, 75)),
            (View, Rgb(51, 71, 86)),
            (Primary, Rgb(201, 214, 224)),
            (Secondary, Rgb(255, 76, 41)),
            (TitlePrimary, Rgb(255, 76, 41)),
            (Highlight, Rgb(255, 76, 41)),
            (HighlightText, Rgb(51, 71, 86)),
        ];
        palette.extend(colours);
        proc.set_theme(Theme {
            shadow: false,
            borders: BorderStyle::Simple,
            palette,
        });
    } else {
        proc.set_theme(Theme {
            shadow: false,
            borders: BorderStyle::Simple,
            palette: Palette::default(),
        })
    }
    proc.set_user_data(UData {
        config,
        video_info: VideoInfo::default(),
        search_results: vec![],
        search_type_radio: RadioGroup::default(),
        subpage_results: vec![],
        search_page_index: 1,
        local_playlist_list: vec![],
        local_playlist: Default::default(),
        video_result: VideoResult::default(),
        local_pl_video_str: String::new(),
        local_pl_video_list: vec![],
        comments_string: Some(String::new()),
    });
    proc.run();
}

fn search(p: &mut Cursive) {
    p.pop_layer();
    p.add_global_callback(Enter, results);
    let mut radio = RadioGroup::new();
    let v = radio.button(String::from("video"), "Videos ");
    let c = radio.button(String::from("channel"), "Channels ");
    let pl = radio.button(String::from("playlist"), "Playlists ");
    let mut data = p.user_data::<UData>().unwrap().clone();
    data.search_type_radio = radio.clone();
    p.set_user_data(data);
    p.add_layer(
        Dialog::around(
            LinearLayout::vertical()
                .child(EditView::new().with_name("searchbox"))
                .child(DummyView)
                .child(TextView::new("Search for:"))
                .child(LinearLayout::horizontal().child(v).child(c).child(pl)),
        )
            .title("Search")
            .button("Go", results),
    );
}

fn results(p: &mut Cursive) {
    let mut data = p.user_data::<UData>().unwrap().clone();
    let radio = data.search_type_radio.clone();
    let index = data.search_page_index;
    let selected_type = &*radio.selection();
    p.clear_global_callbacks(Enter);
    p.clear_global_callbacks(Esc); // makes sure number of ESC callback is 0
    p.add_global_callback(Esc, |p| {
        p.pop_layer();
    });
    let mut query = String::new();
    p.call_on_name("searchbox", |view: &mut EditView| {
        query = view.get_content().to_string();
    })
        .unwrap();
    let cards;
    match SearchResult::get(query.as_str(), &selected_type, index, &data.config) {
        Ok(v) => cards = v,
        Err(e) => {
            p.add_layer(Dialog::text(format!("{:?}", e)));
            return;
        }
    };
    let videocard_template = |i: usize| -> LinearLayout {
        let current: &VideoResult = match &cards[i] {
            SearchResult::Video(v) => v,
            _ => return LinearLayout::vertical(),
        };
        LinearLayout::vertical()
            .child(LinearLayout::horizontal().child(Button::new(
                String::from(" ") + current.title() + " ",
                |p| {
                    video_menu(p);
                },
            )))
            .child(TextView::new(current.author()))
            .child(TextView::new(seconds_to_minutes(&current.length())))
            .child(TextView::new(current.views().to_string() + " views"))
            .child(TextView::new(current.published()))
            .child(DummyView)
    };
    let playlist_template = |i: usize| -> LinearLayout {
        let current: &PlaylistResult = match &cards[i] {
            SearchResult::Playlist(v) => v,
            _ => return LinearLayout::vertical(),
        };
        LinearLayout::vertical()
            .child(LinearLayout::horizontal().child(Button::new(
                String::from(" ") + current.title() + " ",
                |p| {
                    playlist_view(p);
                },
            )))
            .child(TextView::new(current.author()))
            .child(TextView::new(current.length().to_string() + " videos"))
            .child(DummyView)
    };
    let channel_template = |i: usize| -> LinearLayout {
        let current: &ChannelResult = match &cards[i] {
            SearchResult::Channel(c) => c,
            _ => return LinearLayout::vertical(),
        };
        LinearLayout::vertical()
            .child(LinearLayout::horizontal().child(Button::new(
                String::from(" ") + current.author() + " ",
                |p| channel_view(p),
            )))
            .child(TextView::new(current.description()))
            .child(TextView::new(
                current.subscribers().to_string()
                    + " subscribers | "
                    + &current.videos().to_string()
                    + " videos",
            ))
            .child(DummyView)
    };
    let mut viewcards = LinearLayout::vertical();
    if selected_type == "video" {
        for i in 0..cards.len() {
            viewcards.add_child(videocard_template(i));
        }
    } else if selected_type == "playlist" {
        for i in 0..cards.len() {
            viewcards.add_child(playlist_template(i))
        }
    } else if selected_type == "channel" {
        for i in 0..cards.len() {
            viewcards.add_child(channel_template(i))
        }
    }
    viewcards.add_child(DummyView);
    viewcards.add_child(Button::new("Previous Page", |p| {
        let mut data = p.user_data::<UData>().unwrap().clone();
        let index = data.search_page_index;
        if index <= 1 {
            return;
        }
        data.search_page_index = index - 1;
        p.set_user_data(data);
        p.pop_layer();
        results(p);
    }));
    viewcards.add_child(Button::new("Next Page", |p| {
        let mut data = p.user_data::<UData>().unwrap().clone();
        let index = data.search_page_index;
        data.search_page_index = index + 1;
        p.set_user_data(data);
        p.pop_layer();
        results(p);
    }));
    data.search_results = cards;
    p.set_user_data(data);
    p.add_layer(
        Dialog::around(viewcards.with_name("cards"))
            .title("Results | Page ".to_string() + index.to_string().as_str())
            .scrollable()
            .scroll_y(true),
    );
}

fn video_menu(p: &mut Cursive) {
    p.clear_global_callbacks(Esc);
    p.add_global_callback(Esc, |p| {
        p.pop_layer();
    });
    let mut data = p.user_data::<UData>().unwrap().clone();
    let mut cards = vec![];
    let mut n = 0;
    if let Some(i) = p.call_on_name("pl_contents", |view: &mut LinearLayout| {
        view.get_focus_index()
    }) {
        n = i - 1;
        cards = data.subpage_results.clone();
    } else if let Some(i) = p.call_on_name("ch_contents", |view: &mut LinearLayout| {
        view.get_focus_index()
    }) {
        n = i - 4;
        cards = data.subpage_results.clone();
    } else if let Some(i) =
        p.call_on_name("cards", |view: &mut LinearLayout| view.get_focus_index())
    {
        n = i;
        cards = data.search_results.clone();
    }
    let current = match &cards[n] {
        SearchResult::Video(v) => v.id().clone(),
        _ => String::new(),
    };
    let info;
    match VideoInfo::get(current.as_str(), &data.config) {
        Ok(v) => info = v,
        Err(e) => {
            p.add_layer(Dialog::text(format!("{:?}", e)));
            return;
        }
    }
    data.video_info = info;
    p.set_user_data(data);
    p.add_layer(Dialog::around(
        LinearLayout::vertical()
            .child(Button::new("Play Video", play_video))
            .child(Button::new("Download Video", download_video))
            .child(Button::new("View Info", video_info))
            .child(Button::new("View Comments", comments_view))
            .child(Button::new("View Channel", channel_view))
            .child(Button::new("Download Thumbnail", download_thumb))
            .child(Button::new("Add to Playlist", playlist_add_video)),
    ));
}

fn video_info(p: &mut Cursive) {
    let video = p.user_data::<UData>().unwrap().video_info.clone();
    p.clear_global_callbacks(Esc); // makes sure number of ESC callbacks is 0
    p.add_global_callback(Esc, |p| {
        p.pop_layer();
    });
    let layout = {
        LinearLayout::vertical()
            .child(TextView::new(video.title()))
            .child(TextView::new(
                video.views().to_string() + " 👁️ | " + video.likes().to_string().as_str() + " 👍",
            ))
            .child(TextView::new(
                String::from("Length: ") + seconds_to_minutes(&video.length()).as_str(),
            ))
            .child(TextView::new(
                String::from("Published: ") + video.published().as_str(),
            ))
            .child(TextView::new(String::from(
                video.author().to_string() + " (" + video.subscribers().as_str() + " subscribers)",
            )))
            .child(DummyView)
            .child(DummyView)
            .child(TextView::new(video.description()))
    }
        .full_screen()
        .scrollable()
        .scroll_y(true);
    p.add_layer(layout);
}

fn comments_view(p: &mut Cursive) {
    let mut data = p.user_data::<UData>().unwrap().clone();
    let comments_p;
    match CommentsPage::get(
        data.video_info.id().as_str(),
        data.comments_string,
        &data.config,
    ) {
        Ok(v) => comments_p = v,
        Err(e) => {
            p.add_layer(Dialog::text(format!("{:?}", e)));
            return;
        }
    }
    data.comments_string = comments_p.continuation().clone();
    let comment_template = |i: usize| -> LinearLayout {
        let comment = comments_p.comments()[i].clone();
        let replies = comment.replies().clone().unwrap_or_default();
        let mut l = LinearLayout::vertical()
            .child(TextView::new(
                comment.author().to_string() + {
                    if comment.author_is_channel_owner() {
                        " <channel owner>"
                    } else {
                        ""
                    }
                },
            ))
            .child(TextView::new(comment.content()))
            .child(TextView::new(
                comment.likes().to_string() + "👍 " + comment.published().as_str() + {
                    if comment.is_edited() {
                        " <edited>"
                    } else {
                        ""
                    }
                },
            ))
            .child(DummyView);
        if replies.count() > 1 {
            l.add_child(Button::new(
                (replies.count() - 1).to_string() + " reply(s)",
                move |p| {
                    let mut data = p.user_data::<UData>().unwrap().clone();
                    data.comments_string = Some(replies.continuation().to_string());
                    p.set_user_data(data);
                    comments_view(p);
                },
            ));
        }
        l.add_child(DummyView);
        l
    };
    let mut layout = LinearLayout::vertical();
    for i in 0..(comments_p.comments().len() - 1) {
        layout.add_child(comment_template(i))
    }
    p.set_user_data(data);
    layout.add_child(Button::new("Continue", comments_view));
    p.add_layer(layout.full_screen().scrollable().scroll_y(true));
}

fn playlist_view(p: &mut Cursive) {
    p.clear_global_callbacks(Esc); // makes sure number of ESC callback is 0
    p.add_global_callback(Esc, |p| {
        p.pop_layer();
    });
    let n = p
        .call_on_name("cards", |view: &mut LinearLayout| view.get_focus_index())
        .unwrap();
    let mut data = p.user_data::<UData>().unwrap().clone();
    let cards = data.search_results.clone();
    let current = match &cards[n] {
        SearchResult::Playlist(v) => v.id().clone(),
        _ => String::new(),
    };
    let pl;
    match PlaylistInfo::get(current.as_str(), &data.config) {
        Ok(p) => pl = p,
        Err(e) => {
            p.add_layer(Dialog::text(format!("{:?}", e)));
            return;
        }
    }
    let videos = pl.videos().clone();
    let pl_video_template = |i: usize| -> LinearLayout {
        let current = &videos[i];
        LinearLayout::vertical()
            .child(LinearLayout::horizontal().child(Button::new(
                String::from(" ") + current.title() + " ",
                |p| {
                    video_menu(p);
                },
            )))
            .child(TextView::new(current.author()))
            .child(TextView::new(seconds_to_minutes(&current.length())))
            .child(DummyView)
    };
    let mut viewcards = LinearLayout::vertical().child(
        LinearLayout::vertical()
            .child(TextView::new(format!(
                "By {} ({}/channel/{})",
                pl.author(),
                data.config.instance,
                pl.author_id()
            )))
            .child(TextView::new(format!(
                "Share: {}/playlist?list={}",
                data.config.instance,
                pl.id()
            )))
            .child(TextView::new(format!(
                "{}👁 | {} videos",
                pl.views(),
                pl.length()
            )))
            .child(DummyView),
    );
    for i in 0..pl.videos().len() {
        viewcards.add_child(pl_video_template(i))
    }
    let mut sr_videos = vec![];
    for v in &videos {
        sr_videos.append(&mut vec![SearchResult::Video(v.clone())])
    }
    data.subpage_results = sr_videos;
    p.set_user_data(data.clone());
    p.add_layer(
        Dialog::around(viewcards.with_name("pl_contents"))
            .title(pl.title())
            .scrollable()
            .scroll_y(true),
    )
}

fn channel_view(p: &mut Cursive) {
    p.clear_global_callbacks(Esc); // makes sure number of ESC callback is 0
    p.add_global_callback(Esc, |p| {
        p.pop_layer();
    });
    let n = p
        .call_on_name("cards", |view: &mut LinearLayout| view.get_focus_index())
        .unwrap();
    let mut data = p.user_data::<UData>().unwrap().clone();
    let cards = data.search_results.clone();
    let current = match &cards[n] {
        SearchResult::Channel(c) => c.id().clone(),
        SearchResult::Video(v) => v.author_id().clone(),
        _ => String::new(),
    };
    let ch;
    match ChannelInfo::get(current.as_str(), 1, &data.config) {
        Ok(c) => ch = c,
        Err(e) => {
            p.add_layer(Dialog::text(format!("{:?}", e)));
            return;
        }
    }
    let videos = ch.latest().clone();
    let ch_video_template = |i: usize| -> LinearLayout {
        let current = &videos[i];
        LinearLayout::vertical()
            .child(LinearLayout::horizontal().child(Button::new(
                String::from(" ") + current.title() + " ",
                |p| {
                    video_menu(p);
                },
            )))
            .child(TextView::new(seconds_to_minutes(&current.length())))
            .child(TextView::new(current.views().to_string() + " views"))
            .child(TextView::new(current.published()))
            .child(DummyView)
    };
    let mut viewcards = LinearLayout::vertical()
        .child(TextView::new(ch.description()))
        .child(TextView::new(format!(
            "Share: {}/channel/{}",
            data.config.instance,
            ch.id()
        )))
        .child(TextView::new(format!(
            "{} subscribers | {} views",
            ch.subscribers(),
            ch.views()
        )))
        .child(DummyView);
    for i in 0..videos.len() {
        viewcards.add_child(ch_video_template(i))
    }
    let mut ch_videos = vec![];
    for v in &videos {
        ch_videos.append(&mut vec![SearchResult::Video(v.clone())])
    }
    data.subpage_results = ch_videos;
    p.set_user_data(data.clone());
    p.add_layer(
        Dialog::around(viewcards.with_name("ch_contents"))
            .title(ch.author())
            .scrollable()
            .scroll_y(true),
    );
}

fn play_video(p: &mut Cursive) {
    let data = p.user_data::<UData>().unwrap().clone();
    let mut vformats = p.user_data::<UData>().unwrap().video_info.formats().clone();
    if data.config.yt_dlp {
        // inject yt-dlp URL
        vformats.push(VideoFormats::new_ytdl(
            &data.config.instance,
            data.video_info.id(),
        ));
    }
    let mut layout = LinearLayout::vertical();
    let open_format = move |p: &mut Cursive| {
        let n = p
            .call_on_name("formats", |view: &mut LinearLayout| view.get_focus_index())
            .unwrap();
        let data = p.user_data::<UData>().unwrap().clone();
        let mut video = data.video_info.clone();
        let mut formats = video.formats().clone();
        if data.config.yt_dlp {
            // inject yt-dlp URL
            formats.push(VideoFormats::new_ytdl(
                &data.config.instance,
                data.video_info.id(),
            ));
        }
        formats.reverse();
        let watch_pos_dir = format!(
            "--watch-later-directory={}/watch_positions",
            determine_path().unwrap().0
        );
        match Command::new("mpv")
            .args({
                if data.config.watch_pos {
                    [
                        formats[n].url().as_str(),
                        "--no-terminal",
                        "--save-position-on-quit",
                        watch_pos_dir.as_str(),
                    ]
                } else {
                    [formats[n].url().as_str(), "--no-terminal", "", ""]
                }
            })
            .spawn()
        {
            Ok(_) => {
                p.pop_layer();
            }
            Err(e) => {
                p.add_layer(Dialog::around(TextView::new(
                    format! {"Could not open MPV: {:?}", e},
                )));
            }
        };
    };
    vformats.reverse();
    for f in vformats {
        let name = f
            .quality()
            .0
            .clone()
            .unwrap_or_else(|| String::from("Audio"))
            + " | "
            + f.quality()
            .1
            .clone()
            .unwrap_or_else(|| String::from(""))
            .as_str();
        layout.add_child(Button::new(name, open_format))
    }
    let layout = layout.with_name("formats");
    p.add_layer(Dialog::around(layout))
}

fn download_video(p: &mut Cursive) {
    let mut vformats = p.user_data::<UData>().unwrap().video_info.formats().clone();
    let mut layout = LinearLayout::vertical();
    let open_format = |p: &mut Cursive| {
        let n = p
            .call_on_name("formats", |view: &mut LinearLayout| view.get_focus_index())
            .unwrap();
        let mut video = p.user_data::<UData>().unwrap().video_info.clone();
        let mut formats = video.formats().clone();
        formats.reverse();
        let format_url = formats[n].url().clone();
        let video_title = video.title().clone();
        let format_string = formats[n].quality().0.clone().unwrap_or_default();

        let dir = determine_path().unwrap().0 + "download/";
        let path =
            dir.clone() + sanitise((video_title + "_" + format_string.as_str()).as_str()).as_str();

        {
            let path = path.clone();
            thread::spawn(move || {
                let vid;
                let client = reqwest::blocking::ClientBuilder::new()
                    .timeout(None)
                    .build()
                    .unwrap();
                match client.get(format_url).send() {
                    Ok(r) => {
                        vid = r.bytes().unwrap();
                    }
                    Err(e) => {
                        eprint!("{e}");
                        return;
                    }
                };
                fs::create_dir_all(&dir).unwrap();
                fs::write(&path, &vid).unwrap();
            });
        }

        p.pop_layer();
        p.add_layer(Dialog::info(format!(
            "Download started\nSaving to {}",
            path
        )));
    };
    vformats.reverse();
    for f in vformats {
        let name = f
            .quality()
            .0
            .clone()
            .unwrap_or_else(|| String::from("Audio"))
            + " | "
            + f.quality()
            .1
            .clone()
            .unwrap_or_else(|| String::from(""))
            .as_str();
        layout.add_child(Button::new(name, open_format))
    }
    let layout = layout.with_name("formats");
    p.add_layer(Dialog::around(layout))
}

fn download_thumb(p: &mut Cursive) {
    let video = p
        .user_data::<UData>()
        .unwrap()
        .video_info
        .thumbnail()
        .clone();
    thread::spawn(move || {
        let thumb;
        match reqwest::blocking::get(video) {
            Ok(r) => {
                thumb = r.bytes().unwrap();
            }
            Err(_) => {
                return;
            }
        };
        let dir = determine_path().unwrap().0 + "temp/";
        let now = time::SystemTime::now();
        let since_epoch = now.duration_since(time::UNIX_EPOCH).unwrap().as_secs();
        let path = dir.clone() + since_epoch.to_string().as_str() + "thumb.jpg";
        fs::create_dir_all(&dir).unwrap();
        fs::write(&path, &thumb).unwrap();
        match open::that(&path) {
            Ok(_) => {}
            Err(_) => {}
        };
    });
}

fn playlist_add_video(p: &mut Cursive) {
    let playlist_template = |playlist: &LocalPlaylist| -> LinearLayout {
        LinearLayout::vertical()
            .child(Button::new(playlist.title(), |p| {
                let n = p
                    .call_on_name("pl_cards", |view: &mut LinearLayout| view.get_focus_index())
                    .unwrap();
                let data = p.user_data::<UData>().unwrap().clone();
                let c = data.local_playlist_list;
                let v = data.video_result;
                let ct = c[n].title().clone();
                LocalPlaylist::add(ct.as_str(), &v).unwrap();
                p.pop_layer();
            }))
            .child(TextView::new(playlist.length().to_string() + " videos"))
    };
    let mut data = p.user_data::<UData>().unwrap().clone();
    let mut videos = data.search_results.clone();
    let mut index = p
        .call_on_name("cards", |view: &mut LinearLayout| view.get_focus_index())
        .unwrap();
    let video;
    match &videos[index] {
        SearchResult::Video(v) => video = v.clone(),
        _ => {
            videos = data.subpage_results.clone();
            match p.call_on_name("pl_contents", |view: &mut LinearLayout| {
                view.get_focus_index()
            }) {
                Some(n) => {
                    index = n;
                    match &videos[index] {
                        SearchResult::Video(v) => video = v.clone(),
                        _ => video = VideoResult::default(),
                    }
                }
                None => {
                    index = p
                        .call_on_name("ch_contents", |view: &mut LinearLayout| {
                            view.get_focus_index()
                        })
                        .unwrap()
                        - 4;
                    match &videos[index] {
                        SearchResult::Video(v) => video = v.clone(),
                        _ => video = VideoResult::default(), // disgusting nesting here
                    }
                }
            }
        }
    };
    let current_playlists = match LocalPlaylist::read_all() {
        Ok(v) => v,
        Err(e) => {
            p.add_layer(TextView::new(format!("Error reading playlists: {:?}", e)));
            return;
        }
    };
    let mut cards = LinearLayout::vertical();
    data.local_playlist_list = current_playlists.clone();
    data.video_result = video.clone();
    p.set_user_data(data);
    for pl in current_playlists {
        cards.add_child(playlist_template(&pl))
    }
    cards.add_child(DummyView);
    p.add_layer(
        Dialog::around(cards.with_name("pl_cards").scrollable().scroll_y(true))
            .title("Add to Existing Playlist"),
    );
}

fn playlists_list(p: &mut Cursive) {
    let playlists = match LocalPlaylist::read_all() {
        Ok(v) => v,
        Err(e) => {
            p.add_layer(TextView::new(format!("Error reading playlists: {:?}", e)));
            return;
        }
    };
    let playlist_template = |playlist: &LocalPlaylist| -> LinearLayout {
        LinearLayout::vertical()
            .child(Button::new(playlist.title(), |p| {
                let mut n = p
                    .call_on_name("pl_list_cards", |view: &mut LinearLayout| {
                        view.get_focus_index()
                    })
                    .unwrap()
                    - 2; // -2 to offset other UI elements
                if n != 0 {
                    n /= 2
                }
                let mut data = p.user_data::<UData>().unwrap().clone();
                let c = data.local_playlist_list.clone();
                data.local_playlist = c[n].clone();
                p.set_user_data(data);
                playlist_editor(p);
            }))
            .child(TextView::new(playlist.length().to_string() + " videos"))
    };
    let mut cards = LinearLayout::vertical();
    cards.add_child(
        LinearLayout::vertical()
            .child(EditView::new().with_name("new_pl_name"))
            .child(Button::new("Add New Playlist", |p| {
                let name = p
                    .call_on_name("new_pl_name", |view: &mut EditView| {
                        view.get_content().to_string()
                    })
                    .unwrap();
                LocalPlaylist::add(name.as_str(), &VideoResult::default()).unwrap();
                playlists_list(p);
            })),
    );
    let mut data = p.user_data::<UData>().unwrap().clone();
    data.local_playlist_list = playlists.clone();
    p.set_user_data(data);
    for pl in playlists {
        cards.add_child(DummyView);
        cards.add_child(playlist_template(&pl));
    }
    p.pop_layer();
    p.add_layer(
        Dialog::around(cards.with_name("pl_list_cards").scrollable().scroll_y(true))
            .title("Select Playlist"),
    );
}

fn playlist_editor(p: &mut Cursive) {
    let mut data = p.user_data::<UData>().unwrap().clone();
    let pl = data.local_playlist.clone();
    let cards = pl.videos();
    let videocard_template = |i: usize| -> LinearLayout {
        let current: &VideoResult = &cards[i];
        LinearLayout::vertical()
            .child(LinearLayout::horizontal().child(Button::new(
                String::from(" ") + current.title() + " ",
                |p| {
                    let mut n = p
                        .call_on_name("cards", |view: &mut LinearLayout| view.get_focus_index())
                        .unwrap()
                        - 1;
                    if n == 0 {
                        n /= 2;
                    }
                    let mut data = p.user_data::<UData>().unwrap().clone();
                    data.local_pl_video_str = data.local_pl_video_list[n].id().clone();
                    p.set_user_data(data);
                    local_playlist_menu(p);
                },
            )))
            .child(Button::new("[Delete Video]", |p| {
                let mut n = p
                    .call_on_name("cards", |view: &mut LinearLayout| view.get_focus_index())
                    .unwrap()
                    - 1;
                if n == 0 {
                    n /= 2;
                }
                let name = p.user_data::<UData>().unwrap().local_pl_video_str.clone();
                match LocalPlaylist::delete(name.as_str(), Some(n)) {
                    Ok(()) => {}
                    Err(e) => {
                        p.add_layer(TextView::new(format!("Error reading playlists: {:?}", e)));
                        return;
                    }
                };
                p.pop_layer();
                playlists_list(p);
            }))
            .child(TextView::new(current.author()))
            .child(TextView::new(seconds_to_minutes(&current.length())))
            .child(TextView::new(current.views().to_string() + " views"))
            .child(TextView::new(current.published()))
            .child(DummyView)
    };
    let mut videocards = LinearLayout::vertical().child(Button::new("[Delete Playlist]", |p| {
        let name = p.user_data::<UData>().unwrap().local_pl_video_str.clone();
        match LocalPlaylist::delete(name.as_str(), None) {
            Ok(()) => {}
            Err(e) => {
                p.add_layer(TextView::new(format!("Error reading playlists: {:?}", e)));
                return;
            }
        };
        p.pop_layer();
        playlists_list(p);
    }));
    for i in 0..cards.len() {
        videocards.add_child(videocard_template(i))
    }
    data.local_pl_video_list = cards.clone();
    data.local_pl_video_str = pl.title().clone();
    p.set_user_data(data);
    p.add_layer(
        Dialog::around(videocards.with_name("cards"))
            .title("Videos")
            .scrollable()
            .scroll_y(true),
    );
}

fn local_playlist_menu(p: &mut Cursive) {
    let mut data = p.user_data::<UData>().unwrap().clone();
    let link = data.local_pl_video_str.clone();
    let video;
    match VideoInfo::get(link.as_str(), &data.config) {
        Ok(v) => video = v,
        Err(e) => {
            p.add_layer(Dialog::text(format!("{:?}", e)));
            return;
        }
    };
    data.video_info = video;
    p.set_user_data(data);
    p.add_layer(Dialog::around(
        LinearLayout::vertical()
            .child(Button::new("Play Video", play_video))
            .child(Button::new("Download Video", download_video))
            .child(Button::new("View Info", video_info))
            .child(Button::new("Download Thumbnail", download_thumb)),
    ));
}

fn settings(p: &mut Cursive) {
    let data = p.user_data::<UData>().unwrap().clone();
    p.pop_layer();
    p.add_layer(
        Dialog::around(
            LinearLayout::vertical()
                .child(TextView::new("Select Invidious Instance"))
                .child(
                    Button::new(data.config.instance.clone(), |p| {
                        let mut inst_layout =
                            SelectView::new().on_submit(|p: &mut Cursive, item: &String| {
                                p.pop_layer();
                                p.call_on_name("instance", |view: &mut Button| {
                                    view.set_label(item)
                                })
                                    .unwrap();
                            }); // SelectView could be ported to search results page?
                        match Instance::get() {
                            Ok(instances) => {
                                for i in instances {
                                    if !i.api() {
                                        continue;
                                    }
                                    inst_layout.add_item(
                                        format!("{} [{}]", i.url(), i.region()),
                                        i.url().clone(),
                                    )
                                }
                                p.add_layer(
                                    Dialog::around(inst_layout.scrollable().scroll_y(true))
                                        .title("Select Instance"),
                                )
                            }
                            Err(e) => p.add_layer(TextView::new(format!(
                                "Error fetching Invidious Instance list: {:?}",
                                e
                            ))),
                        }
                    })
                        .with_name("instance"),
                )
                .child(DummyView)
                .child(TextView::new("Locale Preference"))
                .child(EditView::new().with_name("locale").max_width(3))
                .child(TextView::new("Save Watch Positions"))
                .child(Checkbox::new().with_name("watch_pos"))
                .child(TextView::new("Dark Mode"))
                .child(Checkbox::new().with_name("dark_mode"))
                .child(TextView::new("Use yt-dlp Playback (HD video)"))
                .child(Checkbox::new().with_name("yt-dlp"))
                .child(LinearLayout::vertical().child(Button::new(
                    "Delete Cached Downloads",
                    |p| {
                        if let Ok(pth) = determine_path() {
                            fs::remove_dir_all(pth.0.to_string() + "temp").unwrap_or(());
                            p.add_layer(Dialog::around(
                                LinearLayout::vertical()
                                    .child(TextView::new("Cached Downloads Cleared!"))
                                    .child(Button::new("OK", |p| {
                                        p.pop_layer();
                                    })),
                            ))
                        } else {
                            p.add_layer(Dialog::around(TextView::new(
                                "Error Clearing Temp Directory",
                            )));
                        };
                    },
                )))
                .child(LinearLayout::vertical().child(Button::new(
                    "Clear Watch Position History",
                    |p| {
                        if let Ok(pth) = determine_path() {
                            fs::remove_dir_all(pth.0.to_string() + "watch_positions").unwrap_or(());
                            p.add_layer(Dialog::around(
                                LinearLayout::vertical()
                                    .child(TextView::new("Watch Positions Cleared!"))
                                    .child(Button::new("OK", |p| {
                                        p.pop_layer();
                                    })),
                            ))
                        } else {
                            p.add_layer(Dialog::around(TextView::new(
                                "Error Clearing Watch Positions",
                            )));
                        };
                    },
                )))
                .child(Button::new("Save Settings", |p| {
                    Config {
                        instance: p
                            .call_on_name("instance", |view: &mut Button| {
                                view.label()[1..view.label().len() - 1].to_string()
                            }) // Strip < > from button label
                            .unwrap(),
                        locale: p
                            .call_on_name("locale", |view: &mut EditView| {
                                view.get_content().to_string()
                            })
                            .unwrap(),
                        watch_pos: p
                            .call_on_name("watch_pos", |view: &mut Checkbox| view.is_checked())
                            .unwrap(),
                        dark_mode: p
                            .call_on_name("dark_mode", |view: &mut Checkbox| view.is_checked())
                            .unwrap(),
                        yt_dlp: p
                            .call_on_name("yt-dlp", |view: &mut Checkbox| view.is_checked())
                            .unwrap(),
                    }
                        .write()
                        .unwrap();
                    p.quit();
                    main();
                }))
                .scrollable()
                .scroll_y(true),
        )
            .title("Settings"),
    );
    p.call_on_name("instance", |view: &mut Button| {
        view.set_label(data.config.instance)
    });
    p.call_on_name("locale", |view: &mut EditView| {
        view.set_content(&data.config.locale);
    });
    p.call_on_name("watch_pos", |view: &mut Checkbox| {
        view.set_checked(data.config.watch_pos);
    });
    p.call_on_name("dark_mode", |view: &mut Checkbox| {
        view.set_checked(data.config.dark_mode);
    });
    p.call_on_name("yt-dlp", |view: &mut Checkbox| {
        view.set_checked(data.config.yt_dlp);
    });
}
