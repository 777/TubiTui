/*
    TubiTui - A lightweight, libre, TUI-based YouTube client
    Copyright (C) 2021 777 <g1t_777@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
*/

use crate::search::VideoResult;
use crate::utils::Errs;
use crate::Config;
use serde::Deserialize;
use std::error::Error;

fn get(id: &str, instance: &str, locale: &str, page: usize) -> Result<String, reqwest::Error> {
    let response = reqwest::blocking::get(format!(
        "{}/api/v1/channels/{}?region={}&page={}",
        instance, id, locale, page
    ))?
    .text()?;
    Ok(response)
}

#[derive(Deserialize, Default, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ChannelInfo {
    author: String,
    author_id: String,
    //author_thumbnails: Vec<VideoThumbs>,
    sub_count: usize,
    total_views: usize,
    description: String,
    latest_videos: Vec<VideoResult>,
}

impl ChannelInfo {
    pub fn get(id: &str, page: usize, conf: &Config) -> Result<ChannelInfo, Errs> {
        let json = match get(id, conf.instance.as_str(), conf.locale.as_str(), page) {
            Ok(r) => r,
            Err(e) => {
                return if let Some(code) = e.status() {
                    Err(Errs::NetworkError(code.to_string()))
                } else {
                    Err(Errs::NetworkError(e.source().unwrap().to_string()))
                }
            }
        };
        match serde_json::from_str(json.as_str()) {
            Ok(r) => Ok(r),
            Err(e) => Err(Errs::PageError(e.to_string())),
        }
    }
    pub fn author(&self) -> &String {
        &self.author
    }
    pub fn id(&self) -> &String {
        &self.author_id
    }
    /*pub fn pfp(&self) -> &String {
        &self.author_thumbnails[0].url
    }*/
    pub fn subscribers(&self) -> usize {
        self.sub_count
    }
    pub fn views(&self) -> usize {
        self.total_views
    }
    pub fn description(&self) -> &String {
        &self.description
    }
    pub fn latest(&self) -> &Vec<VideoResult> {
        &self.latest_videos
    }
}
