/*
    TubiTui - YouTube client with a Text User Interface
    Copyright (C) 2021 777 <g1t_777@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
*/

use crate::utils::Errs;
use serde::Deserialize;
use std::error::Error;

#[derive(Deserialize, Clone)]
#[serde(transparent)]
pub struct Instance {
    i: Vec<Inner>,
}

#[derive(Deserialize, Default, Clone)]
struct InstanceInner {
    region: String,
    api: Option<bool>,
    uri: String,
}

#[derive(Deserialize, Clone)]
#[serde(untagged)]
enum Inner {
    Inst(InstanceInner),
    Name(String),
}

impl Instance {
    pub fn get() -> Result<Vec<Instance>, Errs> {
        let json = match reqwest::blocking::get("https://api.invidious.io/instances.json") {
            Ok(r) => r,
            Err(e) => {
                return if let Some(code) = e.status() {
                    Err(Errs::NetworkError(code.to_string()))
                } else {
                    Err(Errs::NetworkError(e.source().unwrap().to_string()))
                }
            }
        }
        .text()
        .unwrap();
        match serde_json::from_str(json.as_str()) {
            Ok(r) => Ok(r),
            Err(e) => Err(Errs::PageError(e.to_string())),
        }
    }
    pub fn region(&self) -> &String {
        match &self.i.get(1).unwrap() {
            Inner::Inst(i) => &i.region,
            Inner::Name(n) => n,
        }
    }
    pub fn api(&self) -> bool {
        match &self.i.get(1).unwrap() {
            Inner::Inst(i) => i.api.unwrap_or(false),
            Inner::Name(_n) => false,
        }
    }
    pub fn url(&self) -> &String {
        match &self.i.get(1).unwrap() {
            Inner::Inst(i) => &i.uri,
            Inner::Name(n) => n,
        }
    }
}
