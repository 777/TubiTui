/*
    TubiTui - A lightweight, libre, TUI-based YouTube client
    Copyright (C) 2021 777 <g1t_777@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
*/

use crate::config::Config;
use crate::utils::Errs;
use serde::Deserialize;
use std::error::Error;

fn get(id: &str, instance: &str, locale: &str) -> Result<String, reqwest::Error> {
    let response = reqwest::blocking::get(format!(
        "{}/api/v1/videos/{}?region={}",
        instance, id, locale
    ))?
    .text()?;
    Ok(response)
}

#[derive(Deserialize, Default, Clone)]
#[serde(rename_all = "camelCase")]
pub struct VideoInfo {
    title: String,
    video_id: String,
    video_thumbnails: Vec<VideoThumbs>,
    description: String,
    published_text: String,
    view_count: usize,
    like_count: usize,
    author: String,
    //author_id: String,
    sub_count_text: String,
    length_seconds: usize,
    format_streams: Vec<VideoFormats>,
}

impl VideoInfo {
    pub fn get(id: &str, conf: &Config) -> Result<VideoInfo, Errs> {
        let json = match get(id, conf.instance.as_str(), conf.locale.as_str()) {
            Ok(r) => r,
            Err(e) => {
                return if let Some(code) = e.status() {
                    Err(Errs::NetworkError(code.to_string()))
                } else {
                    Err(Errs::NetworkError(e.source().unwrap().to_string()))
                }
            }
        };
        match serde_json::from_str(json.as_str()) {
            Ok(r) => Ok(r),
            Err(e) => Err(Errs::PageError(e.to_string())),
        }
    }
    pub fn title(&self) -> &String {
        &self.title
    }
    pub fn id(&self) -> &String {
        &self.video_id
    }
    pub fn thumbnail(&self) -> &String {
        &self.video_thumbnails[0].url
    }
    pub fn description(&self) -> &String {
        &self.description
    }
    pub fn published(&self) -> &String {
        &self.published_text
    }
    pub fn views(&self) -> usize {
        self.view_count
    }
    pub fn likes(&self) -> usize {
        self.like_count
    }
    pub fn author(&self) -> &String {
        &self.author
    }
    /*pub fn author_id(&self) -> &String {
        &self.author_id
    }*/
    pub fn subscribers(&self) -> &String {
        &self.sub_count_text
    }
    pub fn length(&self) -> usize {
        self.length_seconds
    }
    pub fn formats(&mut self) -> &Vec<VideoFormats> {
        &self.format_streams
    }
}

#[derive(Deserialize, Default, Clone)]
#[serde(rename_all = "camelCase")]
pub struct VideoFormats {
    url: String,
    encoding: Option<String>,
    quality_label: Option<String>,
}

impl VideoFormats {
    pub fn new_ytdl(instance: &str, id: &str) -> Self {
        let url = {
            let protocol = instance.split("://").collect::<Vec<&str>>()[0];
            if protocol == "https" {
                // several invidious instances have non-standard pages that ytdlp cannot handle, so must stream through youtube.com
                format!("ytdl://{}/watch?v={}", "youtube.com", id)
            } else {
                // Don't touch non-https instance URLs
                format!("{}/watch?v={}", instance, id)
            }
        };

        VideoFormats {
            url,
            encoding: Some("ytdlp".to_owned()),
            quality_label: Some("best".to_owned()),
        }
    }
    pub fn url(&self) -> &String {
        &self.url
    }
    pub fn quality(&self) -> (&Option<String>, &Option<String>) {
        (&self.quality_label, &self.encoding)
    }
}

#[derive(Deserialize, Default, Clone)]
pub struct VideoThumbs {
    pub url: String,
}
