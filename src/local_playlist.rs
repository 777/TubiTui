/*
    TubiTui - A lightweight, libre, TUI-based YouTube client
    Copyright (C) 2021 777 <g1t_777@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
*/

use crate::{search::VideoResult, utils::determine_path};
use serde::{Deserialize, Serialize};
use std::{fs, io::BufReader};

#[derive(Deserialize, Serialize, Default, Clone)]
pub struct LocalPlaylist {
    name: String,
    videos: Vec<VideoResult>,
}

#[derive(Deserialize, Serialize, Default)]
struct PLContainer {
    playlists: Vec<LocalPlaylist>,
}

impl LocalPlaylist {
    pub fn read_all() -> Result<Vec<LocalPlaylist>, std::io::Error> {
        let dir = determine_path()?.0;
        let path = dir.clone() + "playlists.json";
        if fs::metadata(&path).is_err() {
            fs::create_dir_all(&dir)?;
            fs::write(
                &path,
                serde_json::to_string(&PLContainer { playlists: vec![] }).unwrap(),
            )?;
            return Ok(vec![]);
        }
        let file = fs::File::open(&path)?;
        let reader = BufReader::new(file);
        let container: PLContainer = serde_json::from_reader(reader).unwrap();
        Ok(container.playlists)
    }
    pub fn add(pl_name: &str, video: &VideoResult) -> Result<(), std::io::Error> {
        let path = determine_path()?.0 + "playlists.json";
        let file = fs::File::open(&path)?;
        let reader = BufReader::new(file);
        let mut container: PLContainer = serde_json::from_reader(reader).unwrap();
        let mut current = container.playlists;
        let mut index = 0;
        let mut new_pl = true;
        for (i, pl) in current.iter().enumerate() {
            if pl.name == pl_name {
                index = i;
                new_pl = false;
            }
        }
        let mut prev;
        if new_pl {
            // if trying to add to pl that doesn't already exist, add it
            current.append(&mut vec![LocalPlaylist {
                name: pl_name.to_string(),
                videos: vec![],
            }])
        } else {
            prev = current.remove(index);
            prev.videos.append(&mut vec![video.clone()]);
            current.append(&mut vec![prev]);
        };
        container = PLContainer { playlists: current };
        let new_json = serde_json::to_string(&container).unwrap();
        fs::write(path, new_json)?;
        Ok(())
    }
    pub fn delete(pl_name: &str, index: Option<usize>) -> Result<(), std::io::Error> {
        let path = determine_path()?.0 + "playlists.json";
        let file = fs::File::open(&path)?;
        let reader = BufReader::new(file);
        let mut container: PLContainer = serde_json::from_reader(reader).unwrap();
        let mut current = container.playlists;
        let mut pl_index = 0;
        for (i, pl) in current.iter().enumerate() {
            if pl.name == pl_name {
                pl_index = i;
            }
        }
        if let Some(i) = index {
            let mut videos = current.remove(pl_index);
            videos.videos.remove(i);
            current.append(&mut vec![videos]);
        } else {
            current.remove(pl_index);
        }
        container = PLContainer { playlists: current };
        let new_json = serde_json::to_string(&container).unwrap();
        fs::write(path, new_json)?;
        Ok(())
    }
    pub fn title(&self) -> &String {
        &self.name
    }
    pub fn length(&self) -> usize {
        self.videos.len()
    }
    pub fn videos(&self) -> &Vec<VideoResult> {
        &self.videos
    }
}
