/*
    TubiTui - YouTube client with a Text User Interface
    Copyright (C) 2021 777 <g1t_777@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
*/

use crate::{Config, LocalPlaylist, RadioGroup, SearchResult, VideoInfo, VideoResult};

#[cfg(not(target_os = "windows"))]
pub fn determine_path() -> Result<(String, String), std::io::Error> {
    let home = home::home_dir().unwrap().to_str().unwrap().to_string();
    let dir = home.clone() + "/.tubitui/";
    let cfgpath = dir.clone() + "config.json";
    Ok((dir, cfgpath))
}

#[cfg(target_os = "windows")]
pub fn determine_path() -> Result<(String, String), std::io::Error> {
    let home = home::home_dir().unwrap().to_str().unwrap().to_string();
    let dir = home.clone() + "/AppData/Roaming/tubitui/";
    let cfgpath = dir.clone() + "config.json";
    Ok((dir, cfgpath))
}

#[derive(Debug)]
pub enum Errs {
    NetworkError(String),
    PageError(String),
    NoResults,
}

#[derive(Clone)]
pub struct UData {
    pub config: Config,
    pub video_info: VideoInfo,
    pub search_results: Vec<SearchResult>,
    pub search_type_radio: RadioGroup<String>,
    pub subpage_results: Vec<SearchResult>,
    pub search_page_index: usize,
    pub comments_string: Option<String>,
    pub local_playlist_list: Vec<LocalPlaylist>,
    pub local_playlist: LocalPlaylist,
    pub video_result: VideoResult,
    pub local_pl_video_str: String,
    pub local_pl_video_list: Vec<VideoResult>,
}

pub fn seconds_to_minutes(secs: &usize) -> String {
    // converts seconds to formatted time string
    let h = secs / 3600;
    let m = (secs / 60) % 60;
    let s = secs % 60;
    if h == 0 {
        format!("{:02}:{:02}", m, s)
    } else {
        format!("{}:{:02}:{:02}", h, m, s)
    }
}
